document.addEventListener('DOMContentLoaded', async () => {
  const booksSelect = document.getElementById('books');
  const chapterSelect = document.getElementById('chapter');
  const bookTitle = document.getElementById('book-name');
  const bookChapterTitle = document.getElementById('book-chapter');
  const bookText = document.getElementById('text');
  const versionSelect = document.getElementById('version');
  const button = document.getElementById('search');

  const bookListUrl = {
    local: 'http://localhost:3000/local/first',
    develop: 'https://18y7timb6h.execute-api.us-east-1.amazonaws.com/develop/first',
    master: 'https://jyf5vhradh.execute-api.us-east-1.amazonaws.com/master/first',
  };

  const chapterUrl = {
    local: 'http://localhost:3000/local/chapter',
    develop: 'https://18y7timb6h.execute-api.us-east-1.amazonaws.com/develop/chapter',
    master: 'https://jyf5vhradh.execute-api.us-east-1.amazonaws.com/master/chapter',
  };

  const bookList = [];

  // ==========================================================================================
  // api calls
  // ==========================================================================================

  fetch(bookListUrl.master, { mode: 'cors' })
    .then((res) => res.json())
    .then(async (res) => {
      for (prop in res) {
        bookList.push(res[prop]);
      }

      loadBooksSelect(res.por);
      loadChaptersSelect(res.por);

      let selectValue = booksSelect.value.split('-');
      let book_id = selectValue[0];
      let chapter_id = chapterSelect.value;
      let book_order = selectValue[1];
      let curVersion = versionSelect.value;
      const curLang = curVersion.substr(0, 3);

      loadChapter(book_id, chapter_id, book_order, curVersion, curLang);
    });

  const loadChapter = (book, chapter, bookOrder, version, lang) => {
    fetch(
      chapterUrl.master +
        `?book_id=${book}&chapter_id=${chapter}&book_order=${bookOrder}&version=${version}&lang=${lang}`,
    )
      .then((res) => res.json())
      .then((res) => {
        loadText(res.data);
      });
  };

  // ==========================================================================================
  // functions
  // ==========================================================================================

  const loadBooksSelect = (element) => {
    for (let i = 0; i < element.length; i++) {
      const cont = i;
      const curElement = element[i];
      const curElementId = curElement.book_id;
      const curElementBookName = curElement.book_name;
      const curElementBookOrder = curElement.book_order;

      booksSelect.innerHTML += `<option class=${`book-option-${cont}`} value="${curElementId}-${curElementBookOrder}">${curElementBookName}</option>`;
    }
  };

  const loadChaptersSelect = (element) => {
    chapterSelect.innerHTML = '';

    let value = booksSelect.value.split('-');
    let curId = value[0];
    let numberOfChapters = 0;

    for (let i = 0; i < element.length; i++) {
      if (element[i].book_id == curId) {
        numberOfChapters = element[i].number_of_chapters;
      }
    }

    for (let i = 1; i <= numberOfChapters; i++) {
      chapterSelect.innerHTML =
        chapterSelect.innerHTML +
        (i === 1
          ? `
        <option value="${i}" selected>${i}</option>`
          : `<option value="${i}">${i}</option>`);
    }
  };

  const loadText = (array) => {
    bookText.innerHTML = '';

    bookTitle.textContent = booksSelect.options[booksSelect.selectedIndex].text;
    bookChapterTitle.textContent =
      chapterSelect.options[chapterSelect.selectedIndex].text;

    for (let i = 0; i < array.length; i++) {
      let curElement = array[i];

      let verseNumber = curElement.verse_id;
      let verseText = curElement.verse_text;

      text.innerHTML += `
            <div class="row-verse">            
                <span class="verse-num">${verseNumber}</span>
                <span class="verse-content">${verseText}</span>
            </div>
        `;
    }
  };

  const replaceOptionsTextContent = (array) => {
    const bookOptionsComponentLength = booksSelect.childElementCount;

    for (let i = 0; i < bookOptionsComponentLength; i++) {
      const curOption = document.querySelector(`.book-option-${i}`);
      const curBookName = array[i].book_name;
      curOption.textContent = curBookName;
    }
  };

  // ==========================================================================================
  // event listeners
  // ==========================================================================================

  booksSelect.addEventListener('change', () => {
    loadChaptersSelect(bookList[0]);
  });

  button.addEventListener('click', () => {
    const curValue = booksSelect.value.split('-');
    const curBook = curValue[0];
    const curBookOrder = curValue[1];
    const curChapter = chapterSelect.value;
    const curVersion = versionSelect.value;
    const curLang = curVersion.substr(0, 3);

    loadChapter(curBook, curChapter, curBookOrder, curVersion, curLang);
  });

  versionSelect.addEventListener('change', () => {
    const curVersion = versionSelect.value;
    const curVersionLanguage = curVersion.substr(0, 3);

    switch (curVersionLanguage) {
      case 'POR':
        replaceOptionsTextContent(bookList[0]);
        break;

      case 'ENG':
        replaceOptionsTextContent(bookList[1]);
        break;

      case 'SPN':
        replaceOptionsTextContent(bookList[2]);
        break;

      case 'FRN':
        replaceOptionsTextContent(bookList[3]);
        break;

      case 'ITA':
        replaceOptionsTextContent(bookList[4]);
        break;

      case 'HBR':
        replaceOptionsTextContent(bookList[5]);
        break;

      case 'JPN':
        replaceOptionsTextContent(bookList[6]);
        break;

      case 'CH1':
        replaceOptionsTextContent(bookList[7]);
        break;

      default:
        break;
    }
  });
});
